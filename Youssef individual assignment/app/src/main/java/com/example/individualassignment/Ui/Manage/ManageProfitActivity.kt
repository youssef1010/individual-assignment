package com.example.individualassignment.Ui.Manage

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Switch
import androidx.activity.viewModels
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.individualassignment.Adapter.ProfitDetailsAdapter
import com.example.individualassignment.Model.Profit
import com.example.individualassignment.R
import com.example.individualassignment.Ui.Add.AddProfitActivity
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ManageProfitActivity : AppCompatActivity() {

    private val viewModel: ManageProfitActivityViewModel by viewModels()
    private var profits = ArrayList<Profit>()
    private lateinit var profitDetailsAdapter: ProfitDetailsAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_profit)
        setSupportActionBar(findViewById(R.id.toolbar))

        //TODO MAAK HIER EEN STRING VAN
        supportActionBar?.title = "Manage profits"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        findViewById<FloatingActionButton>(R.id.fabAddProfit).setOnClickListener {
            val intent = Intent(this, AddProfitActivity::class.java)
            startActivity(intent)

        }

        recyclerView = findViewById(R.id.rvAllProfits)
        profits = arrayListOf()

        profitDetailsAdapter = ProfitDetailsAdapter(profits,
            { dialogBox -> buttonPressed(dialogBox) })
        viewManager = LinearLayoutManager(this)
        observeViewModel()

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = profitDetailsAdapter
        }


        createItemTouchHelper().attachToRecyclerView(recyclerView)


    }

    private fun observeViewModel() {
        viewModel.profits.observe(this, Observer { games ->
            this@ManageProfitActivity.profits.clear()
            this@ManageProfitActivity.profits.addAll(games)
            profitDetailsAdapter.notifyDataSetChanged()

        })
    }



    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> { // Used to identify when the user has clicked the back button
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun buttonPressed(profit: Profit) {


        val builder = AlertDialog.Builder(this)
        val dialogLayout = layoutInflater.inflate(R.layout.content_edit_profit, null)
        builder.setView(dialogLayout)

        var dayFormat: SimpleDateFormat? = SimpleDateFormat("d")
        var monthFormat: SimpleDateFormat? = SimpleDateFormat("M")
        var yearFormat: SimpleDateFormat? = SimpleDateFormat("yyyy")

        builder.setTitle("Beheer inkomsten")
        builder.setMessage("Sla veranderingen op?")

        dialogLayout.findViewById<EditText>(R.id.etEditName).setText(profit.name)
        val simpleSwitch = dialogLayout.findViewById<View>(R.id.swEditConsistent) as Switch
        simpleSwitch.isChecked = profit.consistent

        dialogLayout.findViewById<EditText>(R.id.etEditAmount).setText(profit.amount.toString())

        dialogLayout.findViewById<EditText>(R.id.etEditStartDay).setText(dayFormat?.format(profit.startDate))
        dialogLayout.findViewById<EditText>(R.id.etEditStartMonth).setText(monthFormat?.format(profit.startDate))
        dialogLayout.findViewById<EditText>(R.id.etEditStartYear).setText(yearFormat?.format(profit.startDate))

        dialogLayout.findViewById<EditText>(R.id.etEditEndDay).setText(dayFormat?.format(profit.endDate))
        dialogLayout.findViewById<EditText>(R.id.etEditEndMonth).setText(monthFormat?.format(profit.endDate))
        dialogLayout.findViewById<EditText>(R.id.etEditEndYear).setText(yearFormat?.format(profit.endDate))


        builder.setPositiveButton("ja", { dialogInterface: DialogInterface, i: Int ->

            profit.name = dialogLayout.findViewById<EditText>(R.id.etEditName).text.toString()
            profit.amount = dialogLayout.findViewById<EditText>(R.id.etEditAmount).text.toString().toInt()
            profit.consistent = simpleSwitch.isChecked


            var startDay = dialogLayout.findViewById<EditText>(R.id.etEditStartDay).text.toString().toInt()
            var startMonth = dialogLayout.findViewById<EditText>(R.id.etEditStartMonth).text.toString().toInt()-1
            var startYear = dialogLayout.findViewById<EditText>(R.id.etEditStartYear).text.toString().toInt()

            var endDay = dialogLayout.findViewById<EditText>(R.id.etEditEndDay).text.toString().toInt()
            var endMonth = dialogLayout.findViewById<EditText>(R.id.etEditEndMonth).text.toString().toInt()-1
            var endYear = dialogLayout.findViewById<EditText>(R.id.etEditEndYear).text.toString().toInt()

            var profitStartDate = Calendar.getInstance()
            profitStartDate.set(startYear, startMonth, startDay)

            var profitEndDate = Calendar.getInstance()
            profitEndDate.set(endYear,endMonth,endDay)


            profit.startDate = profitStartDate.time
            profit.endDate = profitEndDate.time

            println(profit.name)
            println(profit.amount)
            println(endYear)


            viewModel.updateProfit(profit)


            finish()
        })

        builder.setNegativeButton("nee", { dialogInterface: DialogInterface, i: Int -> })
        builder.show()


    }


    private fun createItemTouchHelper(): ItemTouchHelper {

        // Callback which is used to create the ItemTouch helper. Only enables left swipe.
        // Use ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) to also enable right swipe.
        val callback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            // Enables or Disables the ability to move items up and down.
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            // Callback triggered when a user swiped an item.
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val reminderToDelete = profits[position]

                viewModel.deleteProfit(reminderToDelete)
            }
        }
        return ItemTouchHelper(callback)
    }


}