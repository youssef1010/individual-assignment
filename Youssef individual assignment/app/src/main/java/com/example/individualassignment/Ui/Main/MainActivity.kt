package com.example.individualassignment.Ui.Main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.individualassignment.Adapter.ProfitAdapter
import com.example.individualassignment.Model.Profit
import com.example.individualassignment.R
import com.example.individualassignment.Ui.Manage.ManageProfitActivity

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {


    private lateinit var profits: ArrayList<Profit>
    private lateinit var profitAdapter: ProfitAdapter
    private lateinit var recyclerViewProfits: RecyclerView
    private lateinit var viewManagerProfits: RecyclerView.LayoutManager

    private var date = Calendar.getInstance();
    private val viewModel: MainActivityViewModel by viewModels()

    var monthFormat: SimpleDateFormat = SimpleDateFormat("M")
    var yearFormat: SimpleDateFormat = SimpleDateFormat("yyyy")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        recyclerViewProfits = findViewById(R.id.rvProfits)
        profits = arrayListOf()
        profitAdapter = ProfitAdapter(profits)
        viewManagerProfits = LinearLayoutManager(this)


        observeViewModelProfits()

        recyclerViewProfits.apply {
            setHasFixedSize(true)
            layoutManager = viewManagerProfits
            adapter = profitAdapter
        }

        btnChangeDate.setOnClickListener { view ->
            setMonth()
        }
        initViews()


    }

    private fun initViews() {
        btnEditProfits.setOnClickListener {
            val intent = Intent(this, ManageProfitActivity::class.java)
            startActivity(intent)
        }
        setTextViews()
    }

    private fun setTextViews() {


        var dateString = profitAdapter.simpleDateFormat(date.time)
        tvDate.text = getString(R.string.current_date, dateString)

        if (profits.isNotEmpty()) {
            viewModel.updateProfit(profits[0]);

        }


    }

    private fun setMonth() {
        var startMonth = etSetMonth.text.toString().toInt() -1
        var startYear = etSetYear.text.toString().toInt()
        date.set(startYear, startMonth, startMonth)

        setTextViews()

    }

    private fun getProfitOfMonth(list: List<Profit>, positiveIncome: Boolean): List<Profit> {
        var filteredList: ArrayList<Profit> = ArrayList()
        val iterate = list.listIterator()
        while (iterate.hasNext()) {
            var holder = iterate.next()
            var currentDate = date;

            var currentYear = yearFormat?.format(currentDate.time).toInt()
            var currentMonth = monthFormat?.format(currentDate.time).toInt()

            var startYear = yearFormat?.format(holder.startDate.time).toInt()
            var endYear = yearFormat?.format(holder.endDate.time).toInt()

            var startMonth = monthFormat.format(holder.startDate.time)
                .toInt()
            var endMonth = monthFormat?.format(holder.endDate.time)
                .toInt()


            var addCondition: Boolean = false


            // als startyear groter is dan momenteel jaar en endyear kleiner is dan momenteel jaar, add altijd
            if (startYear < currentYear &&
                endYear > currentYear
            ) {
                addCondition = true;
            }

            // als startyear gelijk is aan momenteel jaar en endyear groter is dan  momenteel jaar:
            // als startmonth kleiner of gelijk is aan momentele maand add

                if (startYear == currentYear &&
                    endYear > currentYear
                ) {
                    if (startMonth <= currentMonth
                    ) {
                        println("start is: "+ startMonth + "current is: "+currentMonth)
                        println(holder.name+" komt hier")
                        addCondition = true;
                    }

                }



            // als startyear gelijk is aan momenteel jaar en endyear gelijk is aan  momenteel jaar:
            // als startmonth kleiner of gelijk is aan momentele maand en endmonth kleiner of gelijk is aan momentele maand add


                if (startYear == currentYear &&
                    endYear == currentYear
                ) {
                    if (startMonth >= currentMonth &&
                        endMonth >= currentMonth
                    ) {
                        addCondition = true;
                    }
                }

                if (startYear < currentYear &&
                    endYear == currentYear
                ) {
                    println("end is: "+ endMonth + "current is: "+currentMonth)

                    if (endMonth >= currentMonth
                    ) {
                        addCondition = true;
                    }
                }

//                if (startYear < currentYear &&
//                    endYear > currentYear
//                ) {
//                    if (startMonth >= currentMonth
//                    ) {
//                    }
//                }



            if (addCondition) {
                filteredList.add(holder)
            }

        }
        return filteredList;
    }





    private fun observeViewModelProfits() {
        viewModel.profits.observe(this, Observer { viewModelProfits ->
            this@MainActivity.profits.clear()
            this@MainActivity.profits.addAll(getProfitOfMonth(viewModelProfits, true))
            profitAdapter.notifyDataSetChanged()
            setTextViews()

        })
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }


}
