package com.example.individualassignment.Ui.Add

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.individualassignment.Database.ProfitRepository
import com.example.individualassignment.Model.Profit
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AddProfitActivityViewModel(application: Application) : AndroidViewModel(application) {

    private val profitRepository = ProfitRepository(application.applicationContext)
    private val mainScope = CoroutineScope(Dispatchers.Main)


    val error = MutableLiveData<String?>()
    val success = MutableLiveData<Boolean>()

    fun insertGame(profit:Profit){
        if (isProfitValid(profit)) {
            mainScope.launch {
                withContext(Dispatchers.IO) {
                    profitRepository.insertProfit(profit)
                }
            }

            success.value=true;
        }
    }

    private fun isProfitValid(profit:Profit): Boolean {
        return when {
            profit == null -> {
                error.value = "profit may not be null."
                false
            }
            profit.name=="" -> {
                error.value = "Title must not be empty"
                false
            }
            else -> true
        }
    }
}

