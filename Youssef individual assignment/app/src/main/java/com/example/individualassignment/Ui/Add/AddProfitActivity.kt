package com.example.individualassignment.Ui.Add

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Switch
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.individualassignment.Model.Profit
import com.example.individualassignment.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.content_add_profit.*
import java.util.*


class AddProfitActivity : AppCompatActivity() {

    private val viewModel: AddProfitActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_profit)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = "Voeg inkomst toe"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        findViewById<FloatingActionButton>(R.id.fabAddProfit).setOnClickListener {

            createProfit()

        }
    }

    private fun createProfit() {
        val simpleSwitch =
            findViewById<View>(R.id.swConsistent) as Switch
        val switchState = simpleSwitch.isChecked


        var name = etName.text.toString();
        var consistent = switchState
        var profit = switchState;
        var amount = etAmount.text.toString().toInt()

        var startDay =etStartDay.text.toString().toInt()
        var startMonth =etStartMonth.text.toString().toInt()-1
        var startYear = etStartYear.text.toString().toInt()

        var startDate = Calendar.getInstance()
        startDate.set(startYear,startMonth,startDay)

        var endDay =etEndDay.text.toString().toInt()
        var endMonth =etEndMonth.text.toString().toInt()-1
        var endYear = etEndYear.text.toString().toInt()

        var endDate = Calendar.getInstance()
        endDate.set(endYear,endMonth,endDay)

        var addProfit = Profit(name, consistent, profit, amount, startDate.time, endDate.time)

        viewModel.insertGame(addProfit)
        finish()
    }



override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    return when (item?.itemId) {
        android.R.id.home -> { // Used to identify when the user has clicked the back button
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}





}