package com.example.individualassignment.Database

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.individualassignment.Model.Profit
import java.util.*

class ProfitRepository(context: Context) {

    private val profitDao: ProfitDao

    init {
        val database = ProfitRoomDataBase.getDatabase(context)
        profitDao = database!!.profitDao()
    }


    fun getAllRecords() : LiveData<List<Profit>> {
        return profitDao.getAllRecords()
    }

    fun getProfitOnDate(date: Date) : LiveData<List<Profit>> {
        return profitDao.getProfitsOnDate(date)
    }

    suspend fun updateProfit(profit: Profit) {
        profitDao.updateProfit(profit)
    }

    suspend fun insertProfit(profit: Profit) {
        profitDao.insertProfit(profit)
    }

    suspend fun deleteProfit(profit: Profit) {
        profitDao.deleteProfit(profit)
    }

}