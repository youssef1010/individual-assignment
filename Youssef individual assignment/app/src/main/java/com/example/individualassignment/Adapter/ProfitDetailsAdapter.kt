package com.example.individualassignment.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.individualassignment.Model.Profit
import com.example.individualassignment.R
import kotlinx.android.synthetic.main.item_profit_details.view.*
import java.text.SimpleDateFormat
import java.util.*

public class ProfitDetailsAdapter(
    private val profits: List<Profit>,
    private val onClick: (Profit) -> Unit
) :
    RecyclerView.Adapter<ProfitDetailsAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_profit_details, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return profits.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(profits[position])
    }

    fun dateFormat(receivedDate: Date): String {
        var pattern = "MMM-d-yyyy"
        var format: SimpleDateFormat? = SimpleDateFormat(pattern)
        var formatDate = format?.format(receivedDate).toString()
        return formatDate
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener { onClick(profits[adapterPosition]) }
        }

        fun bind(profit: Profit) {
            itemView.tvDetailedNamie.text = profit.name;
            itemView.tvDetailedAmount.text = profit.amount.toString();
            if (profit.consistent) {
                itemView.tvDetailedType.text = "consistent"
            } else {
                itemView.tvDetailedType.text = "eenmalig"
            }
            itemView.tvDetailedStartDate.text = dateFormat(profit.startDate)
            itemView.tvDetailedEndDate.text = dateFormat(profit.endDate)

        }
    }
}