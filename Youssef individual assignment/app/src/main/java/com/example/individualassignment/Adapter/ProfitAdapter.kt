package com.example.individualassignment.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.individualassignment.Model.Profit
import com.example.individualassignment.R
import kotlinx.android.synthetic.main.item_profit.view.*
import java.text.SimpleDateFormat
import java.util.*

public class ProfitAdapter(private val matches: List<Profit>) :
    RecyclerView.Adapter<ProfitAdapter.ViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_profit, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return matches.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            holder.bind(matches[position])
    }



    fun simpleDateFormat(receivedDate: Date): String {
        var pattern ="MMM-yyyy"
        var format: SimpleDateFormat? = SimpleDateFormat(pattern)
        var formatDate = format?.format(receivedDate).toString()
        return formatDate
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(profit : Profit) {
            itemView.tvName.text = profit.name;
            itemView.tvAmount.text = profit.amount.toString();
            if(profit.consistent){
                itemView.tvType.text = "consistent"
            }
            else{
                itemView.tvType.text = "eenmalig"
            }

        }
    }
}