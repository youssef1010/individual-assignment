package com.example.individualassignment.Model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity
data class Profit(

    var name: String,
    var consistent: Boolean,
    var profit: Boolean,
    var amount: Int,
    var startDate:Date,
    var endDate:Date,
    @PrimaryKey var id : Long? = null
) : Parcelable

