package com.example.individualassignment.Ui.Manage

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.individualassignment.Database.ProfitRepository
import com.example.individualassignment.Model.Profit
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ManageProfitActivityViewModel(application: Application) : AndroidViewModel(application) {

    private val profitRepository = ProfitRepository(application.applicationContext)
    private val mainScope = CoroutineScope(Dispatchers.Main)

    val profits: LiveData<List<Profit>> = profitRepository.getAllRecords()


    val error = MutableLiveData<String?>()
    val success = MutableLiveData<Boolean>()

    fun insertProfit(profit:Profit){
        println("arrived")
        if (isProfitValid(profit)) {
            mainScope.launch {
                withContext(Dispatchers.IO) {
                    profitRepository.insertProfit(profit)
                }
            }
            success.value=true;
        }
    }

    private fun isProfitValid(profit:Profit): Boolean {
        return when {
            profit == null -> {
                error.value = "profit may not be null."
                false
            }
            profit.name=="" -> {
                error.value = "Title must not be empty"
                false
            }
            else -> true
        }
    }

    fun updateProfit(profit:Profit){
        if (isProfitValid(profit)) {
            mainScope.launch {
                withContext(Dispatchers.IO) {
                    profitRepository.updateProfit(profit)
                }
            }
            success.value=true;
        }
    }

    fun deleteProfit(profit:Profit){
        mainScope.launch {
            withContext(Dispatchers.IO) {
                profitRepository.deleteProfit(profit)
            }
        }
    }
}

