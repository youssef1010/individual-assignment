package com.example.individualassignment.Database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.individualassignment.Model.Profit
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

@TypeConverters(Converters::class)
@Database(entities = [Profit::class], version = 1, exportSchema = false)
abstract class ProfitRoomDataBase : RoomDatabase() {
    abstract fun profitDao(): ProfitDao
    companion object {
        private const val DATABASE_NAME = "PROFIT_DATABASE"

        @Volatile
        private var INSTANCE: ProfitRoomDataBase? = null

        fun getDatabase(context: Context): ProfitRoomDataBase? {
            if (INSTANCE == null) {
                synchronized(ProfitRoomDataBase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            ProfitRoomDataBase::class.java, DATABASE_NAME
                        )
                            .fallbackToDestructiveMigration()
                            .addCallback(object : RoomDatabase.Callback() {
                                override fun onCreate(db: SupportSQLiteDatabase) {
                                    super.onCreate(db)
                                    INSTANCE?.let { database ->
                                        CoroutineScope(Dispatchers.IO).launch {

                                            var startDate = Calendar.getInstance()
                                            var endDate = Calendar.getInstance()
                                            startDate.set(2020,6,10)
                                            endDate.set(2020,6,20)


                                            database.profitDao().insertProfit(Profit("testRecord Bijbaan", true, true,500,startDate.time,endDate.time))
                                        }
                                    }
                                }
                            })

                            .build()
                    }
                }
            }
            return INSTANCE
        }
    }
}
