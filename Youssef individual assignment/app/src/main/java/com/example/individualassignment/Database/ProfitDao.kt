package com.example.individualassignment.Database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.individualassignment.Model.Profit
import java.util.*

@Dao
interface ProfitDao {

    @Insert
    suspend fun insertProfit(profit: Profit)

    @Query("SELECT * FROM Profit LIMIT 1")
    fun getProfit(): LiveData<Profit?>

    @Query("SELECT * FROM Profit WHERE profit IS 1")
    fun getAllProfits(): LiveData<Profit?>

    @Query("SELECT * FROM Profit WHERE profit IS 0")
    fun getAllExpenditures(): LiveData<Profit?>

    @Query("SELECT * FROM Profit WHERE startDate > :date AND endDate <:date ORDER BY amount ASC")
    fun getProfitsOnDate(date: Date): LiveData<List<Profit>>

    @Query("SELECT * FROM Profit ORDER BY amount ASC")
    fun getAllRecords(): LiveData<List<Profit>>

    @Delete
    suspend fun deleteProfit(profit: Profit)

    @Update
    suspend fun updateProfit(profit: Profit)
}